package com.example.project0418level1

open class BossWarrior: AbstractWarrior(150,12,15,Weapons.rifle) {
    override var isKilled: Boolean = false
}