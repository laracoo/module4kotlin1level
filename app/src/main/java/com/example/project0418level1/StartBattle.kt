package com.example.project0418level1

fun main() {
    println("Введите число войнов в командах: ")
    val number: Int = readLine()?.toIntOrNull()?: return

    val newBattle = Battle(Team(number), Team(number))
    while(!newBattle.isBattleEnd) {
        newBattle.getBattleState()
        newBattle.nextIteration()
    }
}