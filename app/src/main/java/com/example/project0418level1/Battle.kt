package com.example.project0418level1

class Battle(val firstTeam: Team, val secondTeam: Team) {
    var isBattleEnd: Boolean = false

    fun getBattleState(): BattleState {
        var isFirstTeamAlive: Int = 0
        var isSecondTeamAlive: Int = 0
        for (i in 0 until firstTeam.listWarriors.size) {
            if (!firstTeam.listWarriors[i].isKilled) {
                isFirstTeamAlive = +1
            }
        }

        for (i in 0 until secondTeam.listWarriors.size) {
            if (!secondTeam.listWarriors[i].isKilled) {
                isSecondTeamAlive = +1
            }
        }

        return if (isFirstTeamAlive > 0 && isSecondTeamAlive > 0) {
            isBattleEnd = false
            println(BattleState.Progress(commandAHealth = firstTeam.healthOfTeam, commandBHealth = secondTeam.healthOfTeam))
            BattleState.Progress(
                commandAHealth = firstTeam.healthOfTeam,
                commandBHealth = secondTeam.healthOfTeam
            )

        } else return if (isFirstTeamAlive > 0 && isSecondTeamAlive == 0) {
            isBattleEnd = true
            println("Команда A выйграла")
            BattleState.TeamFirstWin
        } else if (isFirstTeamAlive == 0 && isSecondTeamAlive > 0) {
            isBattleEnd = true
            println("Команда B выйграла")
            BattleState.TeamSecondWin
        } else {
            isBattleEnd = true
            println("Ничья")
            BattleState.Draw
        }
    }

    fun nextIteration() {
        firstTeam.listWarriors.shuffle()
        secondTeam.listWarriors.shuffle()
        for (i in 0 until firstTeam.listWarriors.size) {
            if(!firstTeam.listWarriors[i].isKilled && !secondTeam.listWarriors[i].isKilled) {
                firstTeam.listWarriors[i].attack(secondTeam.listWarriors[i])
                firstTeam.healthOfTeam -= firstTeam.listWarriors[i].hitAccuracy
                secondTeam.listWarriors[i].attack(firstTeam.listWarriors[i])
                secondTeam.healthOfTeam -= secondTeam.listWarriors[i].hitAccuracy

            }
        }
    }
}