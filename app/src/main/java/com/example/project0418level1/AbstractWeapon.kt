package com.example.project0418level1

abstract class  AbstractWeapon constructor(val maxCountOfPatrons: Int, val fireType: FireType){
    var listOfAmmo: MutableList<Ammo> = mutableListOf()
    var isPatronsInside: Boolean = false

    abstract fun createPatron() : Ammo

    fun restart() {
        var ammoList: MutableList<Ammo> = mutableListOf()
        while (ammoList.size < maxCountOfPatrons) {
            ammoList.add(createPatron())
        }
        listOfAmmo = ammoList
        isPatronsInside = true
    }

    fun getPatrons(): MutableList<Ammo> {
        val usedPatrons: MutableList<Ammo> = mutableListOf()
        if(listOfAmmo.isNotEmpty()) {
            for(i in 1..fireType.countOfShots) {
                if(listOfAmmo.isEmpty()) {
                    isPatronsInside = false

                    break
                } else {
                    usedPatrons.add(listOfAmmo.removeAt(0))
                }
            }
    } else {
            isPatronsInside = false
        }
        return usedPatrons
     }
}