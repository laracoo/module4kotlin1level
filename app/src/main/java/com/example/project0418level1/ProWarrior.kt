package com.example.project0418level1

open class ProWarrior: AbstractWarrior(120,9,12,Weapons.revolver) {
    override var isKilled: Boolean = false
}