package com.example.project0418level1

open class MasterWarrior: AbstractWarrior(100,6,9,Weapons.machineGun) {
    override var isKilled: Boolean = false
}